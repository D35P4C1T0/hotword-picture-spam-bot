import glob, os, random, re
from pyrogram import Client, filters
from pathlib import Path
from random import randint


def isLucky(odd):
    result = randint(1,odd)
    return True if result == odd else False


def cheer(message):
    return f"COMPLIMENTI @{message.from_user.username}! Ti sei aggiudicato questo speciale premio, sei stato il fortunato su 8192. #shinyhunt"


if __name__ == "__main__":
    app = Client(
        "kuper_bot",
        api_id=os.environ["api_id"],
        api_hash=os.environ["api_hash"],
        bot_token=os.environ["bot_token"],
    )

    media_folder = "app/media"
    hotword = os.environ["hotword"]
    odd = int(os.environ["odd"])
    special_picture = os.environ["special"]

    @app.on_message(filters.regex(hotword, re.I))
    async def send_kuper_pic_callback(client, message):

        random_picture = random.choice(glob.glob("*", root_dir=media_folder))
        Caption = ""

        # Check if we are lucky
        if isLucky(odd):
            random_picture = special_picture
            Caption = cheer(message)

        print(message)
        print(f"##### Sending {random_picture}")
        await app.send_photo(
            message.chat.id, f"{media_folder}/{random_picture}", caption=Caption
        )

    app.run()
